<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

$router->group(['namespace'=>'Api'], static function () use ($router) {

    // ------------------------------------ Comment routes ------------------------------------

    $router->get('comment','CommentController@index');
    $router->post('comment','CommentController@store');

    $router->post('comment/like','CommentController@like');
    $router->post('comment/dislike','CommentController@dislike');

   // ------------------------------------ Rate routes ------------------------------------

    $router->get('rate/average','RateController@average');
    $router->post('rate','RateController@store');
});
