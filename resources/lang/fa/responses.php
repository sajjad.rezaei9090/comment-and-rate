<?php
return [
    'error' => [
        10001 => 'validation error',
        10002 => 'server error',
    ],
    'success' => [
        20000 => 'ok'
    ]
];
