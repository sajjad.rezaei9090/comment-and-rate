<?php

namespace App\Providers;

use App\Interfaces\{CacheRepositoryInterface, CommentRepositoryInterface, RateRepositoryInterface};
use App\Repositories\{CacheRepository, CommentRepository, RateRepository};
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() :void
    {
        $this->app->singleton(
            CommentRepositoryInterface::class,
            CommentRepository::class
        );
        $this->app->singleton(
            RateRepositoryInterface::class,
            RateRepository::class
        );

        $this->app->singleton(
            CacheRepositoryInterface::class,
            CacheRepository::class
        );
    }
}
