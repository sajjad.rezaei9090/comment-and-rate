<?php


namespace App\Repositories;


use App\Models\Rate;

class RateCacheRepository extends CacheRepository
{
    /**
     * Stores rate.
     *
     * @param array $data
     * @return Rate
     */
    public function store(array $data): Rate
    {
        return Rate::updateOrCreate([
            'owner_id' => $data['owner_id'],
            'ratable_id' => $data['ratable_id'],
            'ratable_type' => $data['ratable_type'],
        ], [
            'rate' => $data['rate'],
        ]);
    }

    /**
     * Average rates.
     *
     * @param string $ratableType
     * @param int $ratableId
     * @return mixed
     */
    public function average(string $ratableType, int $ratableId)
    {
        return $this->cache('RATE_AVERAGE', [$ratableType, $ratableId],
            static function () use ($ratableType, $ratableId) {

                return Rate::where('ratable_type', $ratableType)
                    ->where('ratable_id', $ratableId)
                    ->pluck('rate')
                    ->avg();

            });
    }
}
