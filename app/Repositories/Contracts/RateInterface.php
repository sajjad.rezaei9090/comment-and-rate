<?php


namespace App\Repositories\Contracts;


interface RateInterface
{
    public const RATE_AVERAGE = "rate_average_%s_%d";
}
