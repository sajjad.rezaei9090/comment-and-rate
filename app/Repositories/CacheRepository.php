<?php


namespace App\Repositories;


use App\Repositories\Contracts\{CommentInterface, RateInterface};

class CacheRepository implements CommentInterface, RateInterface
{
    public const COMMENT = 'comment';
    public const RATE    = 'rate';

    /**
     * @var CommentCacheRepository
     */
    private static $commentCacheRepository;

    /**
     * @var RateCacheRepository
     */
    private static $rateCacheRepository;

    /**
     * @param $repository
     *
     * @return mixed
     */
    public static function driver($repository)
    {
        switch ($repository) {
            case self::COMMENT:
                if (!self::$commentCacheRepository) {
                    self::$commentCacheRepository = new CommentCacheRepository();
                }
                return self::$commentCacheRepository;
            case self::RATE:
                if (!self::$rateCacheRepository) {
                    self::$rateCacheRepository = new RateCacheRepository();
                }
                return self::$rateCacheRepository;
        }
    }

    /**
     * @param $name
     * @param $attributes
     *
     * @return array
     */
    protected function getCacheKeyAndTtl($name, $attributes) :array
    {
        $cacheKey = sprintf(constant('self::' . $name), ...$attributes);

        return [
            $cacheKey,
            10,
        ];
    }

    /**
     * @param $name
     * @param $attributes
     * @param $callback
     *
     * @return mixed
     */
    public function cache($name, $attributes, $callback)
    {
        [$cacheKey, $ttl] = $this->getCacheKeyAndTtl($name, $attributes);

        // find from redis cache
        if ($response = app('redis')->get($cacheKey)) {
            return json_decode($response);
        }

        // call callback
        $response = $callback();

        // set in redis
        app('redis')->setex($cacheKey, $ttl, json_encode($response));

        return $response;
    }
}
