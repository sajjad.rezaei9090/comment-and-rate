<?php


namespace App\Repositories;


use App\Http\Resources\V1\Comment\CommentResource;
use App\Models\Comment;

class CommentCacheRepository extends CacheRepository
{
    /**
     * List posts by given category.
     *
     * @param string $commentableType
     * @param int    $commentableId
     * @param int    $page
     *
     * @return mixed
     */
    public function index(string $commentableType, int $commentableId, int $page)
    {
        return $this->cache('COMMENT_INDEX', [$commentableType, $commentableId, $page],
            static function () use ($commentableType, $commentableId, $page) {

                $limit  = Comment::PAGINATION;
                $offset = ($page === 1) ? 0 : ($page - 1) * $limit;

                $comments = Comment::where('commentable_type', $commentableType)
                    ->where('commentable_id', $commentableId)
                    ->where('status', Comment::STATUS_CONFIRMED)
                    ->where('parent_id', NULL)
                    ->orderBy('id', 'desc')
                    ->offset($offset)
                    ->limit($limit)
                    ->with([
                        'child' => static function ($query) {
                            $query->where('status', Comment::STATUS_CONFIRMED);
                        },
                    ])
                    ->get();

                return CommentResource::collection($comments);
            });
    }

    /**
     * Stores comment.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function store(array $data)
    {
        return Comment::create($data);
    }

    /**
     * Like comment.
     *
     * @param int $commentId
     *
     * @return void
     */
    public function like( int $commentId) :void
    {
        Comment::find($commentId)
            ->increment('likes');
    }

    /**
     * Dislike comment.
     *
     * @param int $commentId
     *
     * @return void
     */
    public function dislike( int $commentId) :void
    {
        Comment::find($commentId)
            ->increment('dislikes');
    }

}
