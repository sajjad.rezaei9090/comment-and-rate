<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\{Model, Relations\MorphTo, SoftDeletes};

/**
 * Class Rate
 * @package App\Models
 *
 * @property int $id
 *
 * @property int $owner_id
 * @property string $owner_name
 *
 * @property int $ratable_id
 * @property string $ratable_type
 *
 * @property int $rate
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 */
class Rate extends Model
{
    use SoftDeletes;

    /**
     * Ratable types.
     */
    public const RATABLE_TYPE_ARTIST = 'artist';
    public const RATABLE_TYPE_SHOW = 'show';
    public const RATABLE_TYPE_POSTS = 'posts';
    public const RATABLE_TYPES = [
        self::RATABLE_TYPE_ARTIST,
        self::RATABLE_TYPE_SHOW,
        self::RATABLE_TYPE_POSTS,
    ];

    /**
     * Rate min and max constraints.
     */
    public const RATE_MIN = 1;
    public const RATE_MAX = 5;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

   // ------------------------------------ Relations ------------------------------------

    /**
     * Gets the owning ratable model.
     *
     * @return MorphTo
     */
    public function ratable(): MorphTo
    {
        return $this->morphTo();
    }
}
