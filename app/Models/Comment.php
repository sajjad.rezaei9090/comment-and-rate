<?php

namespace App\Models;

use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Database\Eloquent\{Model,
    Relations\BelongsTo,
    Relations\HasMany,
    Relations\HasOne,
    Relations\MorphTo,
    SoftDeletes};

/**
 * Class Comment
 *
 * @package App\Models
 *
 * @property int          $id
 *
 * @property int          $owner_id
 * @property string       $owner_name
 *
 * @property string       $body
 *
 * @property int          $parent_id
 * @property-read Comment $parent
 * @property-read Comment $child
 *
 * @property int          $commentable_id
 * @property string       $commentable_type
 *
 * @property int          $likes
 * @property int          $dislikes
 *
 * @property string       $status
 *
 * @property Carbon       $created_at
 * @property Carbon       $updated_at
 * @property Carbon       $deleted_at
 *
 * @property-read Verta   $formatted_created_at
 * @property-read Verta   $formatted_updated_at
 *
 */
class Comment extends Model
{
    use SoftDeletes;

    /**
     * Comment statuses.
     */
    public const STATUS_STORED    = 'stored';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_REJECTED  = 'rejected';
    public const STATUSES         = [
        self::STATUS_STORED,
        self::STATUS_CONFIRMED,
        self::STATUS_REJECTED,
    ];

    /**
     * Commentable types.
     */
    public const COMMENTABLE_TYPE_ARTIST = 'artist';
    public const COMMENTABLE_TYPE_SHOW   = 'show';
    public const COMMENTABLE_TYPE_POSTS  = 'posts';
    public const COMMENTABLE_TYPE_PLACE  = 'place';
    public const COMMENTABLE_TYPES       = [
        self::COMMENTABLE_TYPE_ARTIST,
        self::COMMENTABLE_TYPE_SHOW,
        self::COMMENTABLE_TYPE_POSTS,
        self::COMMENTABLE_TYPE_PLACE,
    ];

    /**
     * Pagination chunk.
     */
    public const PAGINATION = 10;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $appends = [
        'formatted_created_at',
        'formatted_updated_at',
    ];

    // ------------------------------------ Relations ------------------------------------

    /**
     * Gets the owning commentable model.
     *
     * @return MorphTo
     */
    public function commentable() :MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Return comment which is replied to.
     *
     * @return HasOne
     */
    public function parent() :HasOne
    {
        return $this->HasOne(__CLASS__, 'id', 'parent_id');
    }

    /**
     * Return comment reply.
     *
     * @return HasMany
     */
    public function child() :HasMany
    {
        return $this->hasMany(__CLASS__, 'parent_id', 'id');
    }

    // ------------------------------------ Accessors ------------------------------------

    /**
     * Get Verta formatted created_at.
     *
     * @return string
     */
    public function getFormattedCreatedAtAttribute() :string
    {
        return Verta($this->created_at)->format('j %B Y');
    }

    /**
     * Get Verta formatted updated_at.
     *
     * @return string
     */
    public function getFormattedUpdatedAtAttribute() :string
    {
        return Verta($this->updated_at)->format('j %B Y');
    }
}
