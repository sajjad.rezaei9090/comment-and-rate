<?php


namespace App\Http\Resources\V1\Comment;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CommentResource
 *
 * @package App\Http\Resources\V1\Comment
 *
 * @mixin Comment
 */
class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request) :array
    {
        return [
            'id' => $this->id,

            'body'  => $this->body,
            'reply' => self::collection($this->whenLoaded('child')),

            'owner_name' => $this->owner_name,

            'likes'    => $this->likes,
            'dislikes' => $this->dislikes,

            'created_at' => $this->formatted_created_at,
        ];
    }
}
