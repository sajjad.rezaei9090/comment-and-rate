<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Repositories\CacheRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CommentController extends Controller
{
    /**
     * Shows comments.
     *
     * @param Request $request
     *
     * @return array
     */
    public function index(Request $request) :array
    {
        try {

            // ------------------------------------ Validation ------------------------------------

            $validator = Validator::make($request->all(), [
                'commentable_id'   => 'required|numeric|integer|min:1|exists:comments,commentable_id',
                'commentable_type' => ['required', 'string', Rule::in(Comment::COMMENTABLE_TYPES)],
                'page'             => 'nullable|numeric|integer|min:1',
            ]);

            //Validation fails.
            if ($validator->fails()) {
                return $this->apiResponse(self::ERROR_VALIDATION, '', $validator->getMessageBag());
            }

            $commentableId   = $request->input('commentable_id');
            $commentableType = $request->input('commentable_type');
            $page            = $request->input('page', 1);

            // ------------------------------------ Index ------------------------------------

            $comments = CacheRepository::driver(CacheRepository::COMMENT)
                ->index($commentableType, $commentableId, $page);

            return $this->apiResponse(self::SUCCESS_OK, '', $comments);
        } catch (\Exception $exception) {
            //If anything goes wrong during querying to database.

            return $this->apiResponse(self::ERROR_SERVER, '', $exception->getMessage());
        }
    }

    /**
     * Stores comment.
     *
     * @param Request $request
     *
     * @return array
     */
    public function store(Request $request) :array
    {
        // ------------------------------------ Validation ------------------------------------

        $validator = Validator::make($request->all(), [
            'owner_id'         => 'required|numeric|integer|min:1',
            'owner_name'       => 'nullable|string',
            'commentable_id'   => 'required|numeric|integer|min:1',
            'commentable_type' => ['required', 'string', Rule::in(Comment::COMMENTABLE_TYPES)],
            'parent_id'        => 'nullable|numeric|integer|min:1|exists:comments,id',
            'body'             => 'required|string',
        ]);

        //Validation fails.
        if ($validator->fails()) {
            return $this->apiResponse(self::ERROR_VALIDATION, '', $validator->getMessageBag());
        }

        $input = $request->only([
            'owner_id',
            'owner_name',
            'commentable_id',
            'commentable_type',
            'parent_id',
            'body',
        ]);

        // ------------------------------------ Store ------------------------------------

        try {
            $comment = CacheRepository::driver(CacheRepository::COMMENT)
                ->store($input);

            return $this->apiResponse(self::SUCCESS_OK, '', ['id' => $comment->id]);
        } catch (\Exception $exception) {
            //If anything goes wrong during querying to database.

            return $this->apiResponse(self::ERROR_SERVER, '', $exception->getMessage());
        }
    }

    /**
     * Increment comment likes.
     *
     * @param Request $request
     *
     * @return array
     */
    public function like(Request $request) :array
    {
        // ------------------------------------ Validation ------------------------------------

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|integer|min:1|exists:comments,id',
        ]);

        //Validation fails.
        if ($validator->fails()) {
            return $this->apiResponse(self::ERROR_VALIDATION, '', $validator->getMessageBag());
        }

        $commentId = $request->id;

        // ------------------------------------ Store ------------------------------------

        try {
            CacheRepository::driver(CacheRepository::COMMENT)
                ->like($commentId);

            return $this->apiResponse(self::SUCCESS_OK, '', []);
        } catch (\Exception $exception) {
            //If anything goes wrong during querying to database.

            return $this->apiResponse(self::ERROR_SERVER, '', $exception->getMessage());
        }
    }

    /**
     * Increment comment dislikes.
     *
     * @param Request $request
     *
     * @return array
     */
    public function dislike(Request $request) :array
    {
        // ------------------------------------ Validation ------------------------------------

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|integer|min:1|exists:comments,id',
        ]);

        //Validation fails.
        if ($validator->fails()) {
            return $this->apiResponse(self::ERROR_VALIDATION, '', $validator->getMessageBag());
        }

        $commentId = $request->id;

        // ------------------------------------ Store ------------------------------------

        try {
            CacheRepository::driver(CacheRepository::COMMENT)
                ->dislike($commentId);

            return $this->apiResponse(self::SUCCESS_OK, '', []);
        } catch (\Exception $exception) {
            //If anything goes wrong during querying to database.

            return $this->apiResponse(self::ERROR_SERVER, '', $exception->getMessage());
        }
    }
}
