<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\Repositories\CacheRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RateController extends Controller
{
    /**
     * Stores rate.
     *
     * @param Request $request
     *
     * @return array
     */
    public function store(Request $request) :array
    {
        // ------------------------------------ Validation ------------------------------------

        $validator = Validator::make($request->all(), [
            'owner_id'     => 'required|numeric|integer|min:1',
            'owner_name'   => 'string|nullable',
            'ratable_id'   => 'required|numeric|integer|min:1|exists:rates,ratable_id',
            'ratable_type' => ['required', 'string', Rule::in(Rate::RATABLE_TYPES)],
            'rate'         => 'required|numeric|integer|between:' . Rate::RATE_MIN . ',' . Rate::RATE_MAX,
        ]);

        //Validation fails.
        if ($validator->fails()) {
            return $this->apiResponse(self::ERROR_VALIDATION, '', $validator->getMessageBag());
        }

        $input = $input = $request->only([
            'owner_id',
            'owner_name',
            'ratable_id',
            'ratable_type',
            'rate',
        ]);

        // ------------------------------------ Store ------------------------------------
        try {
            $rate = CacheRepository::driver(CacheRepository::RATE)
                ->store($input);

            return $this->apiResponse(self::SUCCESS_OK, '', ['id' => $rate->id]);
        } catch (\Exception $exception) {
            //If anything goes wrong during querying to database.

            return $this->apiResponse(self::ERROR_SERVER, '', $exception->getMessage());
        }
    }

    /**
     * Sums up rates and outputs average.
     *
     * @param Request $request
     *
     * @return array
     */
    public function average(Request $request) :array
    {
        // ------------------------------------ Validation ------------------------------------

        $validator = Validator::make($request->all(), [
            'ratable_id'   => 'required|numeric|integer|min:1',
            'ratable_type' => ['required', 'string', Rule::in(Rate::RATABLE_TYPES)],
        ]);

        //Validation fails.
        if ($validator->fails()) {
            return $this->apiResponse(self::ERROR_VALIDATION, '', $validator->getMessageBag());
        }

        $ratableType = $request->input('ratable_type');
        $ratableId   = $request->input('ratable_id');

        // ------------------------------------ Gets Data ------------------------------------
        try {
            $data = [
                'average' => CacheRepository::driver(CacheRepository::RATE)
                    ->average($ratableType, $ratableId),
            ];
            return $this->apiResponse(self::SUCCESS_OK, '', $data);
        } catch (\Exception $exception) {
            //If anything goes wrong during querying to database.

            return $this->apiResponse(self::ERROR_SERVER, '', $exception->getMessage());
        }
    }
}
