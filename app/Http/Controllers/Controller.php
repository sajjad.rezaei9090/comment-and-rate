<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //HTTP error status codes.
    /**
     * @const ERROR_VALIDATION
     */
    public const  ERROR_VALIDATION = 10001;
    /**
     * @const ERROR_SERVER
     */
    public const ERROR_SERVER = 10002;

    //HTTP success status codes.
    /**
     * @const SUCCESS_OK
     */
    public const SUCCESS_OK = 20000;

    /**
     * Handles api responses.
     *
     * @param int $code
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function apiResponse(int $code, string $message = '', $data = []): array
    {
        if ($code < 20000) {
            return $this->errorResponse($code, $message, $data);
        }

        return $this->successResponse($code, $message, $data);
    }

    /**
     * Handles api error responses.
     *
     * @param int $code
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function errorResponse(int $code, string $message = '', $data = []): array
    {
        return [
            'success' => false,
            'message' => $message ?: trans("responses.error.{$code}"),
            'code' => $code,
            'data' => $data,
        ];
    }

    /**
     * Handles api success responses.
     *
     * @param int $code
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function successResponse(int $code, string $message = '', $data = []): array
    {
        return [
            'success' => true,
            'message' => $message ?: trans("responses.success.{$code}"),
            'code' => $code,
            'data' => $data,
        ];
    }
}
