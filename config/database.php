<?php


return [
    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'default' => 'mysql_rate',

    'connections' => [
        'mysql_rate' => [
            'driver'         => 'mysql',
            'url'            => env('DATABASE_URL'),
            'host'           => env('MYSQL_RATE_SERVICE_HOST', env('DB_RATE_HOST', env('DB_HOST'))),
            'port'           => env('MYSQL_RATE_SERVICE_PORT', env('DB_RATE_PORT', env('DB_PORT'))),
            'database'       => env('DB_RATE_DATABASE'),
            'username'       => env('DB_RATE_USERNAME'),
            'password'       => env('DB_RATE_PASSWORD'),
            'unix_socket'    => env('DB_SOCKET'),
            'charset'        => 'utf8mb4',
            'collation'      => 'utf8mb4_unicode_ci',
            'prefix'         => '',
            'prefix_indexes' => true,
            'strict'         => true,
            'engine'         => NULL,
            'options'        => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ]

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

//        'cluster' => true,
//        'client'  => 'phpredis',
//        'options' => [
//            'cluster'    => env('REDIS_CLUSTER', 'redis'),
//            'serializer' => 2,
//        ],
//
//        'clusters' => [
//            'default' => [
//                [
//                    'host'     => env('REDIS_CLUSTER_SERVICE_HOST', env('REDIS_HOST', '127.0.0.1')),
//                    'password' => env('REDIS_DYNAMIC_SERVICE_PASSWORD', env('REDIS_PASSWORD', NULL)), //if password otherwise set null
//                    'port'     => env('REDIS_CLUSTER_SERVICE_PORT', env('REDIS_PORT', 7000)),
//                    'database' => 0,
//                ]
//            ],
//        ],

'client' => env('REDIS_CLIENT', 'phpredis'),
'default' => [
    'url'      => env('REDIS_URL'),
    'host'     => env('REDIS_ORDER_SERVICE_HOST', env('REDIS_HOST', '127.0.0.1')),
    'password' => env('REDIS_PASSWORD', NULL),
    'port'     => env('REDIS_ORDER_SERVICE_PORT', env('REDIS_PORT', 6379)),
    'database' => env('REDIS_DB', 0),
],
//        'write' => [
//            'url'      => env('REDIS_URL'),
//            'host'     => env('REDIS_MASTER_SERVICE_HOST', env('REDIS_ORDER_SERVICE_HOST', env('REDIS_HOST', '127.0.0.1'))),
//            'password' => env('REDIS_PASSWORD', NULL),
//            'port'     => env('REDIS_MASTER_SERVICE_PORT', env('REDIS_ORDER_SERVICE_PORT', env('REDIS_PORT', 6379))),
//            'database' => env('REDIS_DB', 0),
//        ],
//        'read' => [
//            'url'      => env('REDIS_URL'),
//            'host'     => env('REDIS_SLAVE_SERVICE_HOST',env('REDIS_ORDER_SERVICE_HOST', env('REDIS_HOST', '127.0.0.1'))),
//            'password' => env('REDIS_PASSWORD', NULL),
//            'port'     => env('REDIS_SLAVE_SERVICE_PORT',env('REDIS_ORDER_SERVICE_PORT', env('REDIS_PORT', 6379))),
//            'database' => env('REDIS_DB', 0),
//        ],
    ],

];
