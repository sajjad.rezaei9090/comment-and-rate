<?php

/** @var Factory $factory */

use App\Models\{Comment, Rate};
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Comment::class, static function (Faker $faker) {
    return [
        'owner_id'         => $faker->randomDigitNotNull,
        'owner_name'       => $faker->name,
        'body'             => $faker->text(200),
        'parent_id'        => NULL,
        'commentable_id'   => $faker->unique()->numberBetween(1, 100),
        'commentable_type' => $faker->randomElement(Comment::COMMENTABLE_TYPES),
        'likes'            => $faker->numberBetween(0, 50),
        'dislikes'         => $faker->numberBetween(0, 50),
        'status'           => Comment::STATUS_CONFIRMED,
    ];
});

$factory->define(Rate::class, static function (Faker $faker) {
    return [
        'owner_id'     => $faker->randomDigitNotNull,
        'owner_name'   => $faker->name,
        'ratable_id'   => $faker->numberBetween(1, 50),
        'ratable_type' => $faker->randomElement(Rate::RATABLE_TYPES),
        'rate'         => $faker->numberBetween(1, 5),
    ];
});
