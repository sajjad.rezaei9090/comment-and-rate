<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() :void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call('CommentsTableSeeder');
        $this->call('RatesTableSeeder');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
