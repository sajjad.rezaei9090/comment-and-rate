<?php

use App\Models\Comment;
use Illuminate\Database\{Migrations\Migration, Schema\Blueprint};
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() :void
    {
        Schema::create('comments', static function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('owner_id')->unsigned()->index();
            $table->string('owner_name')->nullable();

            $table->text('body');

            $table->bigInteger('parent_id')->nullable()->unsigned();
            $table->foreign('parent_id')->references('id')->on('comments')->onDelete('cascade');

            $table->bigInteger('commentable_id')->unsigned()->index();
            $table->enum('commentable_type', Comment::COMMENTABLE_TYPES)->index();

            $table->bigInteger('likes')->unsigned()->default(0);
            $table->bigInteger('dislikes')->unsigned()->default(0);

            $table->enum('status', Comment::STATUSES)->default('stored');

            $table->timestamps();
            $table->softDeletes();

            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() :void
    {
        Schema::dropIfExists('comments');
    }
}
