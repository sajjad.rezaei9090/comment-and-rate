<?php

use Illuminate\Database\{Migrations\Migration, Schema\Blueprint};
use Illuminate\Support\Facades\Schema;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() :void
    {
        Schema::create('rates', static function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('owner_id')->unsigned()->index();
            $table->string('owner_name')->nullable();

            $table->bigInteger('ratable_id')->unsigned()->index();
            $table->enum('ratable_type', ['artist', 'show', 'posts'])->index();

            $table->tinyInteger('rate')->unsigned()->index();

            $table->timestamps();
            $table->softDeletes();

            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() :void
    {
        Schema::dropIfExists('rates');
    }
}
